### draw_polygon

App that allow user to draw a polygon with points touched on the screen.

All the main app logic is inside PolygonDrawerBloc. Communication between UI and PolygonDrawerBloc is done via events.

The "Complete" button is only enabled when there're at least 3 vertices so a polygon can be completed.
When the "Complete" button is pressed, it's once again disabled.


Video:
[Preview](https://drive.google.com/uc?id=1qAkhO4UUAiF06Y9AVTDWC2ZM9aH4-GFG)