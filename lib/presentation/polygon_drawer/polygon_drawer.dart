import 'package:draw_polygon/application/polygon_drawer/polygon_drawer_bloc.dart';
import 'package:draw_polygon/presentation/polygon_drawer/widgets/bottom_button_bar.dart';
import 'package:draw_polygon/presentation/polygon_drawer/widgets/polygon_painter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PolygonDrawer extends StatelessWidget {
  const PolygonDrawer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 90,
          child: BlocBuilder<PolygonDrawerBloc, PolygonDrawerState>(
            builder: (context, state) => GestureDetector(
              child: CustomPaint(
                painter: PolygonPainter(vertices: state.vertices),
                child: Container(),
              ),
              onTapDown: (TapDownDetails tapDownDetails) {
                final localPosition = tapDownDetails.localPosition;
                BlocProvider.of<PolygonDrawerBloc>(context).add(
                  PointAdded(localPosition),
                );
              },
            ),
          ),
        ),
        Expanded(
          flex: 10,
          child: Container(
            color: Colors.blue,
            child: BottomButtonsBar(),
          ),
        )
      ],
    );
  }
}
