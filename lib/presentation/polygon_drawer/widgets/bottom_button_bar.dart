import 'package:draw_polygon/application/polygon_drawer/polygon_drawer_bloc.dart';
import 'package:draw_polygon/presentation/core/widgets/bottom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BottomButtonsBar extends StatelessWidget {
  const BottomButtonsBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        BottomButton(
          text: "Reset",
          disabled: false,
          iconData: Icons.clear,
          onPressed: () =>
              BlocProvider.of<PolygonDrawerBloc>(context).add(PolygonStarted()),
        ),
        BlocBuilder<PolygonDrawerBloc, PolygonDrawerState>(
          builder: (context, state) => BottomButton(
            text: "Complete",
            disabled:
                state.vertices.length < 3 || state is PolygonDrawerDrawComplete,
            iconData: Icons.done_outline,
            onPressed: () => BlocProvider.of<PolygonDrawerBloc>(context)
                .add(PolygonCompleted()),
          ),
        ),
      ],
    );
  }
}
