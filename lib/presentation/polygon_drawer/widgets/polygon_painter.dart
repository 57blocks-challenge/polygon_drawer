import 'dart:ui';

import 'package:flutter/material.dart';

class PolygonPainter extends CustomPainter {
  final List<Offset> _vertices;
  final _myPaint = Paint()
    ..color = Colors.black
    ..strokeWidth = 10
    ..strokeCap = StrokeCap.round;

  PolygonPainter({@required List<Offset> vertices}) : this._vertices = vertices;

  @override
  void paint(Canvas canvas, Size size) {
    if (_vertices.length == 0) return;

    _vertices.length > 1
        ? canvas.drawPoints(PointMode.polygon, _vertices, _myPaint)
        : canvas.drawCircle(_vertices.first, 5, _myPaint);
  }

  @override
  bool shouldRepaint(PolygonPainter oldDelegate) {
    return oldDelegate._vertices.length != _vertices.length;
  }
}
