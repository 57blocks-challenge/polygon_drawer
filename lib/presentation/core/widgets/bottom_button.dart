import 'package:flutter/material.dart';

class BottomButton extends StatelessWidget {
  final String _text;
  final bool _disabled;
  final IconData _iconData;
  final Function _onPressed;

  const BottomButton({
    @required String text,
    @required bool disabled,
    @required IconData iconData,
    @required Function onPressed,
    Key key,
  })  : _text = text,
        _disabled = disabled,
        _iconData = iconData,
        _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      disabledTextColor: Colors.grey,
      onPressed: _disabled ? null : _onPressed,
      label: Text(
        _text,
        style: TextStyle(
          color: _disabled ? Colors.grey : Colors.white,
        ),
      ),
      icon: Icon(
        _iconData,
        color: _disabled ? Colors.grey : Colors.white,
      ),
    );
  }
}
