import 'package:draw_polygon/application/polygon_drawer/polygon_drawer_bloc.dart';
import 'package:draw_polygon/presentation/polygon_drawer/polygon_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Polygon Drawer",
        ),
      ),
      body: BlocProvider<PolygonDrawerBloc>(
        create: (context) => PolygonDrawerBloc(),
        child: PolygonDrawer(),
      ),
    );
  }
}
