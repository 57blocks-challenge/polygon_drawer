import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

part 'polygon_drawer_event.dart';
part 'polygon_drawer_state.dart';

class PolygonDrawerBloc extends Bloc<PolygonDrawerEvent, PolygonDrawerState> {
  PolygonDrawerBloc() : super(PolygonDrawerInitial());

  @override
  Stream<PolygonDrawerState> mapEventToState(
    PolygonDrawerEvent event,
  ) async* {
    if (event is PointAdded) {
      yield* _mapPointAddedToState(event);
    } else if (event is PolygonCompleted) {
      yield* _mapPolygonCompletedToState(event);
    } else if (event is PolygonStarted) {
      yield* _mapPolygonStartedToState();
    }
  }

  Stream<PolygonDrawerState> _mapPointAddedToState(PointAdded event) async* {
    if (state is PolygonDrawerDrawComplete) {
      return;
    }

    final newPoint = event._point;
    final List<Offset> vertices = [...state._vertices, newPoint];

    yield PolygonDrawerDrawInProgress(vertices: vertices);
  }

  Stream<PolygonDrawerState> _mapPolygonCompletedToState(
      PolygonCompleted event) async* {
    final newPoint = state._vertices[0];
    final List<Offset> vertices = [...state._vertices, newPoint];

    yield PolygonDrawerDrawComplete(vertices: vertices);
  }

  Stream<PolygonDrawerState> _mapPolygonStartedToState() async* {
    yield PolygonDrawerInitial();
  }
}
