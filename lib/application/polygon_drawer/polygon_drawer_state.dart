part of 'polygon_drawer_bloc.dart';

abstract class PolygonDrawerState {
  final List<Offset> _vertices;

  List<Offset> get vertices => List<Offset>.unmodifiable(_vertices);

  const PolygonDrawerState({List<Offset> vertices = const []})
      : this._vertices = vertices;
}

class PolygonDrawerInitial extends PolygonDrawerState {}

class PolygonDrawerDrawInProgress extends PolygonDrawerState {
  const PolygonDrawerDrawInProgress({@required List<Offset> vertices})
      : super(vertices: vertices);
}

class PolygonDrawerDrawComplete extends PolygonDrawerState {
  const PolygonDrawerDrawComplete({@required List<Offset> vertices})
      : super(vertices: vertices);
}
