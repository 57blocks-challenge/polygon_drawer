part of 'polygon_drawer_bloc.dart';

abstract class PolygonDrawerEvent {
  const PolygonDrawerEvent();
}

class PointAdded extends PolygonDrawerEvent {
  final Offset _point;

  const PointAdded(this._point);
}

class PolygonStarted extends PolygonDrawerEvent {
  const PolygonStarted();
}

class PolygonCompleted extends PolygonDrawerEvent {
  const PolygonCompleted();
}
